#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

DATASET = "suministro.csv"


def abrir_archivo():
    data = pd.read_csv(DATASET)

    return data


def preg_1(data):
    data.drop(["Animal fats",
               "Animal Products",
               "Aquatic Products, Other",
               "Cereals - Excluding Beer",
               "Eggs",
               "Fish, Seafood",
               "Fruits-Excluding Wine",
               "Milk-Excluding Butter",
               "Miscellaneous",
               "Offals",
               "Oilcrops",
               "Pulses",
               "Spices",
               "Starchy Roots",
               "Stimulants",
               "Sugar Crops",
               "Treenuts",
               "Vegetal Products",
               "Vegetable Oils",
               "Vegetables",
               "Undernourished",
               "Confirmed",
               "Recovered",
               "Active",
               "Population",
               "Sugar & Sweeteners",
               "Unit (all except Population)",
               "Alcoholic Beverages",
               "Meat",
               "Deaths"],
              axis=1,
              inplace=True)

    df = pd.DataFrame(data)
    alto = df.loc[2, "Obesity"]

    for i in range(2, len(df)):

        if df.loc[i, "Obesity"] > df.loc[i-1, "Obesity"] and df.loc[i, "Obesity"] > alto:
            alto = df.loc[i, "Obesity"]
            country = df.loc[i, "Country"]

    print(f"El pais con un mayor indice de obesidad es {country} con un indice de {alto}")


def preg_2(data):

    data.drop(["Animal fats",
               "Animal Products",
               "Aquatic Products, Other",
               "Cereals - Excluding Beer",
               "Eggs",
               "Fish, Seafood",
               "Fruits-Excluding Wine",
               "Milk-Excluding Butter",
               "Miscellaneous",
               "Offals",
               "Oilcrops",
               "Pulses",
               "Spices",
               "Starchy Roots",
               "Stimulants",
               "Sugar Crops",
               "Treenuts",
               "Vegetal Products",
               "Vegetable Oils",
               "Vegetables",
               "Undernourished",
               "Confirmed",
               "Recovered",
               "Active",
               "Population",
               "Sugar & Sweeteners",
               "Unit (all except Population)",
               "Obesity",
               "Meat",
               "Deaths"],
              axis=1,
              inplace=True)

    df = pd.DataFrame(data)
    alto = df.loc[2, "Alcoholic Beverages"]

    for i in range(2, len(df)):

        if df.loc[i, "Alcoholic Beverages"] > df.loc[i-1, "Alcoholic Beverages"] and df.loc[i, "Alcoholic Beverages"] > alto:
            alto = df.loc[i, "Alcoholic Beverages"]
            country = df.loc[i, "Country"]

    print(f"El pais con un mayor indice de consumo de alcohol es {country} con un indice de {alto}")


def preg_3(data):

    data.drop(["Animal fats",
               "Animal Products",
               "Aquatic Products, Other",
               "Cereals - Excluding Beer",
               "Eggs",
               "Fish, Seafood",
               "Fruits-Excluding Wine",
               "Milk-Excluding Butter",
               "Miscellaneous",
               "Offals",
               "Oilcrops",
               "Pulses",
               "Spices",
               "Starchy Roots",
               "Stimulants",
               "Sugar Crops",
               "Treenuts",
               "Vegetal Products",
               "Vegetable Oils",
               "Vegetables",
               "Undernourished",
               "Confirmed",
               "Recovered",
               "Active",
               "Population",
               "Sugar & Sweeteners",
               "Unit (all except Population)",
               "Alcoholic Beverages",
               "Obesity",
               "Deaths"],
              axis=1,
              inplace=True)

    df = pd.DataFrame(data)
    alto = df.loc[2, "Meat"]

    for i in range(2, len(df)):

        if df.loc[i, "Meat"] > df.loc[i-1, "Meat"] and df.loc[i, "Meat"] > alto:
            alto = df.loc[i, "Meat"]
            country = df.loc[i, "Country"]

    print(f"El pais que consume mas carne es {country} con una cantidad {alto}")


def preg_4(data):

    data.drop(["Animal fats",
               "Animal Products",
               "Aquatic Products, Other",
               "Cereals - Excluding Beer",
               "Eggs",
               "Fish, Seafood",
               "Fruits-Excluding Wine",
               "Milk-Excluding Butter",
               "Miscellaneous",
               "Offals",
               "Oilcrops",
               "Pulses",
               "Spices",
               "Starchy Roots",
               "Stimulants",
               "Sugar Crops",
               "Treenuts",
               "Vegetal Products",
               "Vegetable Oils",
               "Vegetables",
               "Undernourished",
               "Confirmed",
               "Recovered",
               "Active",
               "Population",
               "Sugar & Sweeteners",
               "Unit (all except Population)",
               "Alcoholic Beverages",
               "Meat",
               "Obesity"],
              axis=1,
              inplace=True)

    df = pd.DataFrame(data)
    alto = df.loc[2, "Deaths"]

    for i in range(2, len(df)):

        if df.loc[i, "Deaths"] > df.loc[i-1, "Deaths"] and df.loc[i, "Deaths"] > alto:
            alto = df.loc[i, "Deaths"]
            country = df.loc[i, "Country"]

    print(f"El pais con la mayor tasa de muerte a causa del covid-19 es {country} con una tasa de {alto}%")


def main():
    data = abrir_archivo()

    opcion = int(input("""1)¿Cual es el pais con un indice de obsedidad mayor?
    2)¿Cual es el pais en donde hay un mayor indice de alcoholismo?
    3)¿Cual es el pais con mas consumo de carnes?
    4)¿Que pais tiene mas casos de muerte por covid-19?\n"""))

    if opcion == 1:
        preg_1(data)
    elif opcion == 2:
        preg_2(data)
    elif opcion == 3:
        preg_3(data)
    elif opcion == 4:
        preg_4(data)


if __name__ == "__main__":
    main()
